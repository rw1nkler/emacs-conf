#!/bin/bash

EMACS_SOURCE=`pwd`
EMACS_TARGET=~/.emacs.d

if [[ -d $EMACS_TARGET ]] && [[ ! -h $EMACS_TARGET ]]; then
    echo "Directory ${EMACA_TARGET} exists! Moving to ${EMACS_TARGET}.bak..."
    mv ${EMACS_TARGET}/ ${EMACS_TARGET}.bak/
fi

if [[ -h $EMACS_TARGET ]]; then
    LINK_TARGET=`readlink $EMACS_TARGET`
    echo "Symbolic link to ${LINK_TARGET} exists! Moving to ${EMACS_TARGET}.bak..."
    mv ${EMACS_TARGET} ${EMACS_TARGET}.bak
fi

ln -s ${EMACS_SOURCE} ${EMACS_TARGET}
