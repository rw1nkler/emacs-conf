;;
;; INIT_HYDRA.EL
;;

(require 'hydra)

(general-define-key "C-c v" (defhydra hydra-example (:color pink)
"
^Mark^             ^Unmark^          ^Actions^            ^Search^
^----^-------------^------^----------^-------^------------^------^
_m_: mark
"

("m" split-window-below)
)
)

;;(general-define-key "C-c v" #'hydra-example/body)
