;;
;; INIT_COMPANY.EL
;;


(use-package company
  :config
    (global-company-mode 1)
  :general
    (c-mode-base-map "C-c TAB" 'company-complete)
)
