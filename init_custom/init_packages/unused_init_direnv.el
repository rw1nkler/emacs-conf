;;
;; INIT_DIRENV.EL
;;

(use-package direnv
  :config
    (direnv-mode t)
)
