;;
;; INIT_COUNSEL-GTAGS.EL
;;

(use-package counsel-gtags
  :config
    (counsel-gtags-mode t)

  :general
    (:prefix "C-c c"
      "s" 'counsel-gtags-dwim
      "b" 'counsel-gtags-go-backward
      "f" 'counsel-gtags-go-forward
      "d" 'counsel-gtags-find-definition
      "r" 'counsel-gtags-find-reference
    )
)

;;; init_counsel-gtags.el ends here
