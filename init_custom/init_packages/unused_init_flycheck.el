;;
;; INIT_FLYCHECK.EL
;;

(use-package flycheck
  :config
    (global-flycheck-mode 1)
)

