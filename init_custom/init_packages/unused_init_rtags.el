;;
;; INIT_RTAGS.EL
;;

(defun my-flycheck-rtags-setup ()
  (flycheck-select-checker 'rtags)
  ;; RTags creates more accurate overlays.
  (setq-local flycheck-highlighting-mode nil) 
  (setq-local flycheck-check-syntax-automatically nil)
  )

(defun fun-my ()
  (message "ajhksdgfkjasdfkjhasdfkujhasdkjf")
)

(use-package rtags
  :init
    (setq rtags-completions-enabled t)
    (setq rtags-autostart-diagnostics t)
    (setq rtags-display-result-backend 'ivy)

  :config
    (push 'company-rtags company-backends)

  :general
    (:prefix "C-c r"
      "s" 'rtags-find-symbol-at-point
      "b" 'rtags-location-stack-back
      "f" 'rtags-location-stack-forward
      "p" 'rtags-preprocess-file
      "i" 'rtags-get-include-file-for-symbol
    )

    :hook
      (c-mode . rtags-start-process-unless-running)
      (c++-mode . rtags-start-process-unless-running)
      (objc-mode . rtags-start-process-unless-running)
      (c-mode .  my-flycheck-rtags-setup)
      (c++-mode . my-flycheck-rtags-setup)
      (objc-mode  . my-flycheck-rtags-setup)
)

(use-package company
  :config
    (global-company-mode t)
)

(use-package company-rtags)
(use-package flycheck-rtags)

;;; init_rtags.el ends here
