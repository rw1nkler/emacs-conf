;;
;; INIT_PROJECTILE.EL
;;

(use-package projectile
  :init
    (setq projectile-completion-system 'ivy)

    (setq projectile-switch-project-action
      '(lambda ()
	 (venv-projectile-auto-workon)
	 (neotree-projectile-action) 
       ;; NEOTREE
       )
    )

  :hook
    (projectile-before-switch-project . my-fun)

  :config
    (projectile-mode 1)

  :general
    ("C-c p" 'projectile-command-map)
)

(use-package counsel-projectile
  :config
    (counsel-projectile-mode 1)

)

(defun my-fun()
  (message "I found project")
)
