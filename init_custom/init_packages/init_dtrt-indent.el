;;
;;  INIT_DTRT-INDENT.EL
;;

(use-package dtrt-indent
  :hook
  (c-mode . dtrt-indent-mode)
  (c++-mode . dtrt-indent-mode)
  (objc-mode . dtrt-indent-mode)
  (python-mode . dtrt-indent-mode)
)
