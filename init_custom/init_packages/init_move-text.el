;;
;; INIT_MOVE-TEXT.EL
;;

(use-package move-text
  :general
    ("C-M-p" 'move-text-up)
    ("C-M-n" 'move-text-down)
)

