;;
;; INIT_AGGRESSIVE-INDENT.EL
;;

(use-package aggressive-indent
  :hook
    (c-mode . aggressive-indent-mode)
    (c++-mode . aggressive-indent-mode)
)
