;;;
;; INIT_NEOTREE.EL
;;

;; Open smartly neotree with projectile settings
(defun neotree-project-dir ()
    "Open NeoTree using the git root."
    (interactive)
    (let ((project-dir (projectile-project-root))
          (file-name (buffer-file-name)))
      (neotree-toggle)
      (if project-dir
          (if (neo-global--window-exists-p)
              (progn
                (neotree-dir project-dir)
                (neotree-find file-name)))
        (message "Could not find git project root."))))


(use-package neotree
  :init
    ;; Switch project root when switching project with projectile
    ;;(setq projectile-switch-project-action 'neotree-projectile-action)
    
    (setq neo-smart-open t)

  :general
    ("<f8>" 'neotree-project-dir)
)
