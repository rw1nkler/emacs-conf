;;
;; INIT_ACE-WINDOW.EL
;;

(use-package ace-window
  :config
    (ace-window-display-mode 1)
  :general
    ("C-c a" 'ace-window)
)


