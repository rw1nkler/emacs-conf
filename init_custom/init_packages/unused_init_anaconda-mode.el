;;
;; INIT_ANACONDA-MODE.EL
;;

;; Resolves bug with anaconda-mode compilation from MELPA
(require 'rx)

(use-package anaconda-mode
  :init
    (eval-after-load "company" '(add-to-list 'company-backends 'company-anaconda))
  :hook
    (python-mode . anaconda-mode)
    (python-mode . anaconda-eldoc-mode)
)


