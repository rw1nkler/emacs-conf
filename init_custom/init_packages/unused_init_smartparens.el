;;
;; INIT_SMARTPARENS.EL
;;

(use-package smartparens
  :config
    (progn (show-smartparens-global-mode t))

  :hook
    (c-mode . smartparens-mode)
    (c++-mode . smartparens-mode)
)
