;;
;; GENERAL-SETTINGS.EL
;;

;; Disable initial message
(setq inhibit-startup-message t)

;; Auto refreshing files from disk
(global-auto-revert-mode t)

;; Enable globaly lines numbering
(global-linum-mode t)
(setq linum-format "%4d\u2502 ")

;; No menu, toolbar, scrollbar
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; Save session within runs
;; (desktop-save-mode t)

;; Show column numbers
(column-number-mode t)

;; Show whitespaces
(global-whitespace-mode t)

;; No backup files ~
(setq make-backup-files nil)

