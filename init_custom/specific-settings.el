;;
;; SPECIFIC-SETTINGS.EL
;;

;;;; Custom bindings

;; 1. Commenting
  (general-define-key "M-;" 'comment-eclipse)
  (general-define-key "C-x C-b" 'ibuffer)
